package hu.bme.aait.vehicleict.can;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import android.app.Activity;

import hu.bme.aait.vehicleict.can.activity.BluetoothActivity;


@Config(emulateSdk = 18)
@RunWith(RobolectricTestRunner.class)
public class BluetoothActivityTest {

    private BluetoothActivity mActivity;

    @Before
    public void setup() {
        mActivity = Robolectric.buildActivity(BluetoothActivity.class).create().start().resume()
                .get();
    }

    @Test
    public void hasBluetoothAdapter() throws Exception {
        //assertNotNull(mActivity.mBluetoothUtil);
    }

    @Test
    public void testFullCycle() throws Exception {
        Activity activity = Robolectric.buildActivity(BluetoothActivity.class).create().start()
                .resume().visible().pause().stop().destroy().get();
    }
}