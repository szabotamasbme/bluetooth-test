package hu.bme.aait.vehicleict.can.inventure;

import java.util.ArrayList;
import java.util.List;

import hu.bme.aait.vehicleict.can.inventure.sensor.SensorType;

/**
 * Created by Gorog on 2014.10.12.
 */
public class CarSensor {

    private List<SensorData> sensorDatas;

    public CarSensor() {
        sensorDatas = new ArrayList<SensorData>();

        for (SensorType sensorType : SensorType.values()) {
            if (sensorType != SensorType.NaN) {
                sensorDatas.add(new SensorData(null, sensorType));
            }
        }
    }

    public List<SensorData> getSensorDatas() {
        return sensorDatas;
    }

    public SensorData getSensorDataBySensorType(SensorType sensorType) {
        SensorData sensorData = null;
        for (SensorData sensorData1 : getSensorDatas()) {
            if (sensorData1.getSensorType() == sensorType) {
                sensorData = sensorData1;
            }
        }
        return sensorData;
    }

    public void setSensorDataBySensorType(SensorType sensorType, Object value, long time) {
        for (SensorData s : sensorDatas) {
            if (s.getSensorType().equals(sensorType)) {
                s.setValue(value, time);
            }
        }
    }
}
