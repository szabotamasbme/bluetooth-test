package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage502 extends InventureMessage {

    private float acceleratorPedalPosition1;

    private InventureMessage502(float acceleratorPedalPosition1) {
        super();
        this.acceleratorPedalPosition1 = acceleratorPedalPosition1;
    }

    public InventureMessage502(String[] data) {
        this(Float.valueOf(data[0]));
    }

    public float getAcceleratorPedalPosition1() {
        return acceleratorPedalPosition1;
    }

    @Override
    public String toString() {
        return "InventureMessage502{" +
                "acceleratorPedalPosition1=" + acceleratorPedalPosition1 +
                '}';
    }
}
