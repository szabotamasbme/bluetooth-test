package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum TachgraphPerformance {
    NORMAL_PERFORMANCE(0), PERFORMANCE_ANALYSIS(1);

    private final int value;

    TachgraphPerformance(final int value) {
        this.value = value;
    }

    public static TachgraphPerformance fromValue(int value) {
        for (TachgraphPerformance t : TachgraphPerformance.values()) {
            if (t.value == value) {
                return t;
            }
        }
        return null;
    }
}
