package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum PTOState {
    OFF_DISABLED(0), SET(5), NOT_AVAILABLE(31);

    private final int value;

    PTOState(final int value) {
        this.value = value;
    }

    public static PTOState fromValue(int value) {
        for (PTOState p : PTOState.values()) {
            if (p.value == value) {
                return p;
            }
        }
        return null;
    }
}
