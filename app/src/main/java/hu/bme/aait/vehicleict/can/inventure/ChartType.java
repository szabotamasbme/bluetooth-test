package hu.bme.aait.vehicleict.can.inventure;

/**
 * Created by tszabo on 14/10/2014.
 */
public enum ChartType {
    None, Line, Bar, Time, List
}
