package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum DirectionIndicator {
    FORWARD(0), REVERSE(1);

    private final int value;

    DirectionIndicator(final int value) {
        this.value = value;
    }

    public static DirectionIndicator fromValue(int value) {
        for (DirectionIndicator d : DirectionIndicator.values()) {
            if (d.value == value) {
                return d;
            }
        }
        return null;
    }
}
