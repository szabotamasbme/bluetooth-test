package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
 * Created by Gorog on 2014.10.12.
 */
public enum ClutchSwitch {
    PEDAL_RELEASED(0), PEDAL_DEPRESSED(1);

    private final int value;

    ClutchSwitch(final int value) {
        this.value = value;
    }

    public static ClutchSwitch fromValue(int value) {
        for (ClutchSwitch c : ClutchSwitch.values()) {
            if (c.value == value) {
                return c;
            }
        }
        return null;
    }
}
