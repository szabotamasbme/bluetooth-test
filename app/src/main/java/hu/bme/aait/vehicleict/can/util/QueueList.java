package hu.bme.aait.vehicleict.can.util;

import java.util.LinkedList;

/**
 * Created by Gorog on 2014.10.19.
 */
public class QueueList<E> extends LinkedList<E> {

    private int maxSize;

    public QueueList(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public boolean add(E e) {
        super.add(e);
        if (size() > maxSize) {
            super.remove();
        }
        return true;
    }
}
