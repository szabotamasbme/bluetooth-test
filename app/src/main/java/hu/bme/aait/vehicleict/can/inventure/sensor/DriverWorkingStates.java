package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum DriverWorkingStates {
    REST(0), DRIVER_AVAILABLE(1), WORK(2), DRIVE(3), ERROR(6), NOT_AVAILABLE(7);

    private final int value;

    DriverWorkingStates(final int value) {
        this.value = value;
    }

    public static DriverWorkingStates fromValue(int value) {
        for (DriverWorkingStates d : DriverWorkingStates.values()) {
            if (d.value == value) {
                return d;
            }
        }
        return null;
    }
}
