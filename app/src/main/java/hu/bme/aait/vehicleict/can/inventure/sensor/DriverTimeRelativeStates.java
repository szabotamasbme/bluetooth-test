package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum DriverTimeRelativeStates {
    NORMAL(0), _15_MIN_BEF_4_HALF_H(1), _4_HALF_H_REACHED(2), _15_MIN_BEF_9_H(3), _9_H_REACHED(
            4), _15_MIN_BEF_16_H(5), _16_H_REACHED(6), ERROR(14), NOT_AVAILABLE(15);

    private final int value;

    DriverTimeRelativeStates(final int value) {
        this.value = value;
    }

    public static DriverTimeRelativeStates fromValue(int value) {
        for (DriverTimeRelativeStates d : DriverTimeRelativeStates.values()) {
            if (d.value == value) {
                return d;
            }
        }
        return null;
    }
}
