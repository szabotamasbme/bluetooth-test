package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum DriverCard {
    CARD_NOT_PRESENT(0), CARD_PRESENT(1);

    private final int value;

    DriverCard(final int value) {
        this.value = value;
    }

    public static DriverCard fromValue(int value) {
        for (DriverCard d : DriverCard.values()) {
            if (d.value == value) {
                return d;
            }
        }
        return null;
    }
}
