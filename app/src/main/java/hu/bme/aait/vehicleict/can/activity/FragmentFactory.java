package hu.bme.aait.vehicleict.can.activity;

import android.support.v4.app.Fragment;

/**
 * Created by tszabo on 15/10/2014.
 */
public abstract class FragmentFactory {
    private FragmentFactory() {
    }

    public static <T extends Fragment> T getFragment(String name) {
        if (DeviceInfoFragment.TAG.equals(name)) {
            return (T) new DeviceInfoFragment();
        } else if (SensorsFragment.TAG.equals(name)) {
            return (T) new SensorsFragment();
        } else if (VisualisationFragment.TAG.equals(name)) {
            return (T) new VisualisationFragment();
        } else {
            throw new RuntimeException("unknown fragment " + name);
        }
    }
}
