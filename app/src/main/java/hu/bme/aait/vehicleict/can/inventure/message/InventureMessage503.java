package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage503 extends InventureMessage {

    private float engineTotalFuelUsed;

    private InventureMessage503(float engineTotalFuelUsed) {
        super();
        this.engineTotalFuelUsed = engineTotalFuelUsed;
    }

    public InventureMessage503(String[] data) {
        this(Float.valueOf(data[0]));
    }

    public float getEngineTotalFuelUsed() {
        return engineTotalFuelUsed;
    }

    @Override
    public String toString() {
        return "InventureMessage503{" +
                "engineTotalFuelUsed=" + engineTotalFuelUsed +
                '}';
    }
}
