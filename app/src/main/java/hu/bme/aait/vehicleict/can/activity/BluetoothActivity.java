package hu.bme.aait.vehicleict.can.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;
import hu.bme.aait.vehicleict.can.bluetooth.BluetoothAdapterUtil;
import hu.bme.aait.vehicleict.can.bluetooth.BluetoothState;
import hu.bme.aait.vehicleict.can.inventure.CarSensor;
import hu.bme.aait.vehicleict.can.inventure.InventureMessageParser;
import hu.bme.aait.vehicleict.can.inventure.SensorData;
import hu.bme.aait.vehicleict.can.inventure.SensorDataAdapter;
import hu.bme.aait.vehicleict.can.inventure.SensorDataUtil;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage;
import hu.bme.aait.vehicleict.can.inventure.sensor.SensorType;
import hu.bme.aait.vehicleict.can.util.BluetoothDeviceAdapter;
import hu.bme.aait.vehicleict.can.util.QueueList;
import hu.bute.aait.gorog.bluetoothtest.R;


public class BluetoothActivity extends ActionBarActivity
        implements DeviceInfoFragment.DeviceInfoActionListener,
        SensorsFragment.SensorsActionListener, VisualisationFragment.ChartActionListener {

    private static final int REQUEST_ENABLE_BT = 1;

    private static final String ACTIVITY_NAME = "BluetoothActivity";

    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @InjectView(R.id.left_drawer)
    ListView mDrawerList;

    Set<BluetoothDevice> pairedDevices;

    ArrayList<BluetoothDevice> bluetoothDevices;

    BluetoothDeviceAdapter mBluetoothDeviceAdapter;

    List<SensorData> mSensorDatas;

    SensorDataAdapter mSensorDataAdapter;

    BluetoothAdapterUtil mBluetoothUtil;

    private String remoteDeviceAddress;

    private String remoteDeviceName;

    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;

    private CharSequence mTitle;

    private String[] mNavigationTitles = {"Device info", "Sensor data"};

    private CarSensor carSensor;

    private String currentFragment;

    private List<SensorData> sensorDataList;

    private SensorType sensorType;

    private long lastTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        ButterKnife.inject(this);

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mNavigationTitles));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                supportInvalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mBluetoothUtil = new BluetoothAdapterUtil(this);

        sensorDataList = new QueueList<SensorData>(20);
        carSensor = new CarSensor();

        sensorType = SensorType.NaN;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mBluetoothUtil = new BluetoothAdapterUtil(this);

        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        remoteDeviceAddress = sharedPref
                .getString(getString(R.string.saved_remote_device_address), "");
        remoteDeviceName = sharedPref.getString(getString(R.string.saved_remote_device_name), "");

        if (!mBluetoothUtil.isEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if (!mBluetoothUtil.isServiceAvailable()) {
                mBluetoothUtil.setupService();
                mBluetoothUtil.startService(BluetoothState.DEVICE_OTHER);
            }
        }

        mBluetoothUtil.setOnDataReceivedListener(new BluetoothAdapterUtil.OnDataReceivedListener() {
            @Override
            public void onDataReceived(byte[] data, String message) {
                InventureMessage inventureMessage = InventureMessageParser.parse(message);
//                String inventureMessageString = inventureMessage.toString();
                //Log.i(ACTIVITY_NAME, inventureMessageString);
                carSensor = SensorDataUtil.refreshCarSensor(inventureMessage, carSensor);

                if (sensorType.getMessageId() == inventureMessage.getMessageId()) {
                    refreshChart();
                }
                //}
            }
        });

        if (remoteDeviceAddress != null && !remoteDeviceAddress.equals("")) {
            mBluetoothUtil.autoConnect(remoteDeviceName);
        }

        if (currentFragment != null) {
            Fragment fragment = FragmentFactory.getFragment(currentFragment);
            showFragment(fragment, currentFragment);
        } else {
            showFragment(new DeviceInfoFragment(), DeviceInfoFragment.TAG);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.saved_remote_device_address), remoteDeviceAddress);
        editor.putString(getString(R.string.saved_remote_device_name), remoteDeviceName);
        editor.commit();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mBluetoothUtil.disconnect();
        mBluetoothUtil.stopAutoConnect();
        mBluetoothUtil.stopService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(bReceiver);
        } catch (IllegalArgumentException e) {
            Log.i(ACTIVITY_NAME, "Receiver not registered");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("remoteDeviceAddress", remoteDeviceAddress);
        savedInstanceState.putString("currentFragment", currentFragment);
        savedInstanceState.putInt("sensorType", sensorType.ordinal());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            remoteDeviceAddress = savedInstanceState.getString("remoteDeviceAddress");
            currentFragment = savedInstanceState.getString("currentFragment");
            sensorType = SensorType.values()[savedInstanceState.getInt("sensorType")];
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bluetooth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (mBluetoothUtil.isEnabled()) {
                DeviceInfoFragment deviceInfoFragment = (DeviceInfoFragment)
                        getSupportFragmentManager().findFragmentByTag(DeviceInfoFragment.TAG);
                if (deviceInfoFragment != null) {
                    deviceInfoFragment.setButtonsEnabled();
                }
                mBluetoothUtil.setupService();
                mBluetoothUtil.startService(BluetoothState.DEVICE_OTHER);
            } else {
                DeviceInfoFragment deviceInfoFragment = (DeviceInfoFragment)
                        getSupportFragmentManager().findFragmentByTag(DeviceInfoFragment.TAG);
                if (deviceInfoFragment != null) {
                    deviceInfoFragment.setButtonsDisabled();
                }
            }
        }
    }


    public void onDeviceTurnOn() {
        mBluetoothUtil.turnOn();
    }

    public void onDeviceTurnOff() {
        mBluetoothUtil.turnOff();
    }

    public void onDeviceInfoFragmentStarted() {
        if (mBluetoothUtil != null && !mBluetoothUtil.isAvailable()) {
            DeviceInfoFragment deviceInfoFragment = (DeviceInfoFragment)
                    getSupportFragmentManager().findFragmentByTag(DeviceInfoFragment.TAG);
            if (deviceInfoFragment != null) {
                deviceInfoFragment.setButtonsDisabled();
            }
        } else {

            DeviceInfoFragment deviceInfoFragment = (DeviceInfoFragment)
                    getSupportFragmentManager().findFragmentByTag(DeviceInfoFragment.TAG);
            if (deviceInfoFragment != null) {
                deviceInfoFragment.setButtonsEnabled();
            }
            bluetoothDevices = new ArrayList<BluetoothDevice>();
            mBluetoothDeviceAdapter = new BluetoothDeviceAdapter(getApplicationContext(),
                    bluetoothDevices, this);
            deviceInfoFragment.deviceListView.setAdapter(mBluetoothDeviceAdapter);

            if (remoteDeviceAddress != null && !remoteDeviceAddress.equals("")) {
                deviceInfoFragment.setSavedDeviceAddress(remoteDeviceName, remoteDeviceAddress);
            }
        }
    }

    public void onListDevices() {
        bluetoothDevices.clear();
        pairedDevices = mBluetoothUtil.getBondedDevices();

        for (BluetoothDevice device : pairedDevices) {
            bluetoothDevices.add(device);
        }

        mBluetoothDeviceAdapter.notifyDataSetChanged();
    }

    public void onSearchDevices() {
        if (mBluetoothUtil.isDiscovering()) {
            mBluetoothUtil.cancelDiscovery();
        } else {
            bluetoothDevices.clear();
            mBluetoothUtil.startDiscovery();
            registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        }
    }

    public boolean onDeviceSelected(int position) {
        remoteDeviceAddress = mBluetoothDeviceAdapter.getItem(position).getAddress();
        remoteDeviceName = mBluetoothDeviceAdapter.getItem(position).getName();

        mBluetoothUtil.cancelDiscovery();

        mBluetoothUtil.autoConnect(remoteDeviceName);
        DeviceInfoFragment deviceInfoFragment = (DeviceInfoFragment)
                getSupportFragmentManager().findFragmentByTag(DeviceInfoFragment.TAG);
        if (deviceInfoFragment != null) {
            deviceInfoFragment.setSavedDeviceAddress(remoteDeviceName, remoteDeviceAddress);
        }

        return true;
    }

    public void onSensorsFragmentStarted() {
        carSensor = new CarSensor();
        mSensorDatas = carSensor.getSensorDatas();
        mSensorDataAdapter = new SensorDataAdapter(getApplicationContext(), mSensorDatas, this);

        SensorsFragment sensorsFragment = (SensorsFragment)
                getSupportFragmentManager().findFragmentByTag(SensorsFragment.TAG
                );
        sensorsFragment.setListAdapter(mSensorDataAdapter);
    }

    @OnItemClick(R.id.left_drawer)
    public void drawerItemSelected(int position) {
        switch (position) {
            case 0:
                mTitle = mNavigationTitles[0];
                currentFragment = DeviceInfoFragment.TAG;
                showFragment(new DeviceInfoFragment(), DeviceInfoFragment.TAG);
                break;
            case 1:
                mTitle = mNavigationTitles[1];
                currentFragment = SensorsFragment.TAG;
                showFragment(new SensorsFragment(), SensorsFragment.TAG);
                break;
            default:
                break;
        }

        mDrawerLayout.closeDrawer(mDrawerList);
    }

    public void selectChart(SensorType sensorType) {
        showFragment(new VisualisationFragment(), VisualisationFragment.TAG);

        this.sensorType = sensorType;
        SensorData sensorData = carSensor.getSensorDataBySensorType(sensorType);
        SensorData s = new SensorData(sensorData.getValue(),
                sensorData.getSensorType());
        s.setValue(sensorData.getValue(), sensorData.getTime());
        sensorDataList.clear();
        sensorDataList.add(s);
    }

    public void onChartFragmentStarted() {
        refreshChart();
    }

    private void refreshChart() {
        VisualisationFragment visualisationFragment = (VisualisationFragment)
                getSupportFragmentManager().findFragmentByTag(VisualisationFragment.TAG);
        SensorData sensorData = carSensor.getSensorDataBySensorType(sensorType);
        if (sensorData != null) {
            SensorData s = new SensorData(sensorData.getValue(),
                    sensorData.getSensorType());
            s.setValue(sensorData.getValue(), sensorData.getTime());
            sensorDataList.add(s);
        }
        if (visualisationFragment != null) {
            visualisationFragment.setSensorDataList(sensorDataList);
            visualisationFragment.refreshChart();
        }
    }

    private void showFragment(Fragment fragment, String fragmentTag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragmentToRestore = fragmentManager.findFragmentByTag(fragmentTag);
        if (fragmentToRestore != null) {
            fragment = fragmentToRestore;
        }
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, fragmentTag)
                .addToBackStack("tag").commitAllowingStateLoss();
        currentFragment = fragmentTag;
    }

    final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                bluetoothDevices.add(device);
                mBluetoothDeviceAdapter.notifyDataSetChanged();
            }
        }
    };


}
