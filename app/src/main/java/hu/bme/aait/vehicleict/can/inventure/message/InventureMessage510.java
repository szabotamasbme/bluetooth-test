package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage510 extends InventureMessage {

    private int serviceDistance;

    private InventureMessage510(int serviceDistance) {
        super();
        this.serviceDistance = serviceDistance;
    }

    public InventureMessage510(String[] data) {
        this(Integer.valueOf(data[0]));
    }

    public int getServiceDistance() {
        return serviceDistance;
    }

    @Override
    public String toString() {
        return "InventureMessage510{" +
                "serviceDistance=" + serviceDistance +
                '}';
    }
}
