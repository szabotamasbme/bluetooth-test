package hu.bme.aait.vehicleict.can.db;

/**
 * Created by Patrik on 2014.10.26..
 */
public class DbConstants {

    // Broadcast Action, amely az adatbazis modosulasat jelzi
    public static final String ACTION_DATABASE_CHANGED
            = "hu.bme.aait.vehicleict.can.DATABASE_CHANGED";

    // fajlnev, amiben az adatbazis lesz
    public static final String DATABASE_NAME = "Inventure.db";

    //Adatbazis verzio
    public static final int DATABASE_VERSION = 2;

    /* Sensor osztaly DB konstansai */
    public static class Sensor {

        // tabla neve
        public static final String DATABASE_TABLE = "Sensors";

        // oszlopnevek
        public static final String KEY_ROWID = "_id";

        public static final String KEY_TYPE = "type";

        public static final String KEY_VALUE = "value";

        public static final String KEY_TIME = "time";

        // sema letrehozo szkript
        public static final String DATABASE_CREATE =
                "create table if not exists " + DATABASE_TABLE + " ( "
                        + KEY_ROWID + " integer primary key autoincrement, "
                        + KEY_TYPE + " text not null, "
                        + KEY_TIME + " long not null, "
                        + KEY_VALUE + "  text not null, "
                        + "UNIQUE(" + KEY_TYPE + ")"
                        + "); ";

        // sema torlo szkript
        public static final String DATABASE_DROP =
                "drop table if exists " + DATABASE_TABLE + "; ";
    }
}
