package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage508 extends InventureMessage {

    private String vehicleIdentificationNumber;

    private InventureMessage508(String vehicleIdentificationNumber) {
        super();
        this.vehicleIdentificationNumber = vehicleIdentificationNumber;
    }

    public InventureMessage508(String[] data) {
        this(data[0]);
    }

    public String getVehicleIdentificationNumber() {
        return vehicleIdentificationNumber;
    }

    @Override
    public String toString() {
        return "InventureMessage508{" +
                "vehicleIdentificationNumber='" + vehicleIdentificationNumber + '\'' +
                '}';
    }
}
