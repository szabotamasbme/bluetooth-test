package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage507 extends InventureMessage {

    private float engineTotalHoursOfOperation;

    private InventureMessage507(float engineTotalHoursOfOperation) {
        super();
        this.engineTotalHoursOfOperation = engineTotalHoursOfOperation;
    }

    public InventureMessage507(String[] data) {
        this(Float.valueOf(data[0]));
    }

    public float getEngineTotalHoursOfOperation() {
        return engineTotalHoursOfOperation;
    }

    @Override
    public String toString() {
        return "InventureMessage507{" +
                "engineTotalHoursOfOperation=" + engineTotalHoursOfOperation +
                '}';
    }
}
