package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage509 extends InventureMessage {

    private float highResolutionTotalVehicleDistance;

    private InventureMessage509(float highResolutionTotalVehicleDistance) {
        super();
        this.highResolutionTotalVehicleDistance = highResolutionTotalVehicleDistance;
    }

    public InventureMessage509(String[] data) {
        this(Float.valueOf(data[0]));
    }

    public float getHighResolutionTotalVehicleDistance() {
        return highResolutionTotalVehicleDistance;
    }

    @Override
    public String toString() {
        return "InventureMessage509{" +
                "highResolutionTotalVehicleDistance=" + highResolutionTotalVehicleDistance +
                '}';
    }
}
