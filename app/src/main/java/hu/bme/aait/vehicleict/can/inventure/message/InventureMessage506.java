package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage506 extends InventureMessage {

    private float axleWeight1;

    private float axleWeight2;

    private float axleWeight3;

    private float axleWeight4;


    private InventureMessage506(Float axleWeight1, Float axleWeight2, Float axleWeight3,
            Float axleWeight4) {
        this.axleWeight1 = axleWeight1;
        this.axleWeight2 = axleWeight2;
        this.axleWeight3 = axleWeight3;
        this.axleWeight4 = axleWeight4;
    }

    public InventureMessage506(String[] data) {
        this(Float.valueOf(data[0]), Float.valueOf(data[1]), Float.valueOf(data[2]),
                Float.valueOf(data[3]));
    }

    public float getAxleWeight1() {
        return axleWeight1;
    }

    public float getAxleWeight2() {
        return axleWeight2;
    }

    public float getAxleWeight3() {
        return axleWeight3;
    }

    public float getAxleWeight4() {
        return axleWeight4;
    }

    @Override
    public String toString() {
        return "InventureMessage506{" +
                "axleWeight1=" + axleWeight1 +
                ", axleWeight2=" + axleWeight2 +
                ", axleWeight3=" + axleWeight3 +
                ", axleWeight4=" + axleWeight4 +
                '}';
    }
}
