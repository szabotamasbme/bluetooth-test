package hu.bme.aait.vehicleict.can.inventure.sensor;

import java.util.ArrayList;
import java.util.List;

import hu.bute.aait.gorog.bluetoothtest.R;
import hu.bme.aait.vehicleict.can.inventure.ChartType;

/**
 * Created by tszabo on 25/10/2014.
 */
public enum SensorType {
    NaN(0,0,ChartType.None,null),
    WheelBasedSpeed(501, R.string.wheelBasedSpeed, ChartType.Time, Float.class),
    ClutchSwitch(501, R.string.clutchSwitch, ChartType.List, ClutchSwitch.class),
    BrakeSwitch(501, R.string.brakeSwitch, ChartType.List, BrakeSwitch.class),
    CruiseControlActive(501, R.string.cruiseControlActive, ChartType.List, CruiseControlActive.class),
    PTOState(501, R.string.ptoState, ChartType.List, PTOState.class),
    AcceleratorPedalPosition1(502, R.string.acceleratorPedalPosition1, ChartType.Time, Float.class),
    EngineTotalFuelUsed(503, R.string.engineTotalFuelUsed, ChartType.Time, Float.class),
    FuelLevel1(504, R.string.fuelLevel1, ChartType.Time, Float.class),
    EngineSpeed(505, R.string.engineSpeed, ChartType.Time, Float.class),
    AxleWeight1(506, R.string.axleWeight1, ChartType.Time, Float.class),
    AxleWeight2(506, R.string.axleWeight2, ChartType.Time, Float.class),
    AxleWeight3(506, R.string.axleWeight3, ChartType.Time, Float.class),
    AxleWeight4(506, R.string.axleWeight4, ChartType.Time, Float.class),
    EngineTotalHoursOfOperation(507, R.string.engineTotalHoursOfOperation, ChartType.Time, Float.class),
    VehicleIdentificationNumber(508, R.string.vehicleIdentificationNumber, ChartType.List, String.class),
    HighResolutionTotalVehicleDistance(509, R.string.highResolutionTotalVehicleDistance, ChartType.Time, Float.class),
    ServiceDistance(510, R.string.serviceDistance, ChartType.Time, Integer.class),
    VehicleMotion(511, R.string.vehicleMotion, ChartType.List, VehicleMotion.class),
    VehicleOverspeed(511, R.string.vehicleOverspeed, ChartType.List, VehicleOverSpeed.class),
    DirectionIndicator(511, R.string.directionIndicator, ChartType.List, DirectionIndicator.class),
    Driv1WorkingStates(511, R.string.driv1WorkingStates, ChartType.List, DriverWorkingStates.class),
    Driv2WorkingStates(511, R.string.driv2WorkingStates, ChartType.List, DriverWorkingStates.class),
    Driver1Card(511, R.string.driver1Card, ChartType.List, DriverCard.class),
    Driver2Card(511, R.string.driver2Card, ChartType.List, DriverCard.class),
    Driver1TimeRelativeStates(511, R.string.driver1TimeRelativeStates, ChartType.List, DriverTimeRelativeStates.class),
    Driver2TimeRelativeStates(511, R.string.driver2TimeRelativeStates, ChartType.List, DriverTimeRelativeStates.class),
    TachgraphPerformance(511, R.string.tachgraphPerformance, ChartType.List, TachgraphPerformance.class),
    HandlingInformation(511, R.string.handlingInformation, ChartType.List, HandlingInformation.class),
    SystemEvent(511, R.string.systemEvent, ChartType.List, SystemEvent.class),
    TachogrVehicleSpeed(511, R.string.tachogrVehicleSpeed, ChartType.Time, Float.class),
    EngineCoolantTemperature(512, R.string.engineCoolantTemperature, ChartType.Time, Integer.class);

    private int messageId;
    private int nameId;
    private ChartType chartType;
    private Class clazz;

    SensorType(int messageId, int nameId, ChartType chartType, Class clazz) {
        this.messageId = messageId;
        this.nameId = nameId;
        this.chartType = chartType;
        this.clazz = clazz;
    }

    public int getMessageId() {
        return messageId;
    }

    public int getNameId() {
        return nameId;
    }

    public ChartType getChartType() {
        return chartType;
    }

    public Class getClazz() {
        return clazz;
    }

    public static List<SensorType> getSensorsByMessageId(int messageId) {
        List<SensorType> sensorTypeList = new ArrayList<SensorType>();

        for (SensorType s : SensorType.values()) {
            if (s.getMessageId() == messageId) {
                sensorTypeList.add(s);
            }
        }
        return sensorTypeList;
    }

    public static SensorType getSensorTypeByNameId(int nameId) {
        for (SensorType s : values()) {
            if (s.getNameId() == nameId) {
                return s;
            }
        }
        return null;
    }
}
