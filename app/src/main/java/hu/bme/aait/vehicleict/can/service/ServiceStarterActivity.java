package hu.bme.aait.vehicleict.can.service;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemLongClick;
import hu.bme.aait.vehicleict.can.bluetooth.BluetoothAdapterUtil;
import hu.bme.aait.vehicleict.can.bluetooth.BluetoothState;
import hu.bme.aait.vehicleict.can.db.DbConstants;
import hu.bme.aait.vehicleict.can.provider.SensorsProvider;
import hu.bme.aait.vehicleict.can.util.BluetoothDeviceAdapter;
import hu.bute.aait.gorog.bluetoothtest.R;

public class ServiceStarterActivity extends ActionBarActivity {

    private static final String ACTIVITY_NAME = "ServiceStarterActivity";

    @InjectView(R.id.savedDeviceName)
    TextView savedDeviceName;

    @InjectView(R.id.lastEntry)
    TextView lastEntry;

    @InjectView(R.id.serviceButton)
    Button serviceButton;

    @InjectView(R.id.deviceListView)
    ListView deviceListView;

    ArrayList<BluetoothDevice> bluetoothDevices;

    Set<BluetoothDevice> pairedDevices;

    private String remoteDeviceName;

    BluetoothDeviceAdapter mBluetoothDeviceAdapter;

    BluetoothAdapterUtil mBluetoothUtil;

    private boolean serviceRunning;

    SensorsObserver mObserver;

    private SensorsProvider mSensorsProvider;

    Cursor c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_starter);

        ButterKnife.inject(this);

        mBluetoothUtil = new BluetoothAdapterUtil(this);
        bluetoothDevices = new ArrayList<BluetoothDevice>();
        mBluetoothDeviceAdapter = new BluetoothDeviceAdapter(getApplicationContext(),
                bluetoothDevices, this);
        deviceListView.setAdapter(mBluetoothDeviceAdapter);

        serviceRunning = false;

        mSensorsProvider = new SensorsProvider();
        mObserver = new SensorsObserver(new Handler());
        c = getApplicationContext().getContentResolver()
                .query(SensorsProvider.CONTENT_URI, null,
                        DbConstants.Sensor.KEY_ROWID + " = (SELECT MAX("
                                + DbConstants.Sensor.KEY_ROWID + ")  FROM "
                                + DbConstants.Sensor.DATABASE_TABLE + ")",
                        null, null);
        startManagingCursor(c);
        c.registerContentObserver(mObserver);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mBluetoothUtil = new BluetoothAdapterUtil(this);

        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        remoteDeviceName = sharedPref.getString(getString(R.string.saved_remote_device_name), "");

        savedDeviceName.setText(getString(R.string.saved_device_name) + ": " + remoteDeviceName);

        if (!mBluetoothUtil.isEnabled()) {
            Toast.makeText(getApplicationContext(), "Please enable bluetooth!", Toast.LENGTH_LONG)
                    .show();
            finish();
        } else {
            if (!mBluetoothUtil.isServiceAvailable()) {
                mBluetoothUtil.setupService();
                mBluetoothUtil.startService(BluetoothState.DEVICE_OTHER);
            }
        }
        listDevices();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.service_preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.saved_remote_device_name), remoteDeviceName);
        editor.commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBluetoothUtil.disconnect();
        mBluetoothUtil.stopService();
        c.unregisterContentObserver(mObserver);
        stopService(new Intent(this, CarSensorRefreshService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(bReceiver);
        } catch (IllegalArgumentException e) {
            Log.i(ACTIVITY_NAME, "Receiver not registered");
        }
    }

    @OnClick(R.id.serviceButton)
    public void serviceAction() {
        if (!serviceRunning) {
            Intent intent = new Intent(this, CarSensorRefreshService.class);
            intent.putExtra("remoteDeviceName", remoteDeviceName);
            startService(intent);
            serviceRunning = true;
            serviceButton.setText(R.string.stopService);
        } else {
            stopService(new Intent(this, CarSensorRefreshService.class));
            serviceRunning = false;
            serviceButton.setText(R.string.startService);
        }
    }

    @OnItemLongClick(R.id.deviceListView)
    public boolean deviceSelected(int position) {
        remoteDeviceName = mBluetoothDeviceAdapter.getItem(position).getName();

        savedDeviceName.setText(getString(R.string.saved_device_name) + ": " + remoteDeviceName);

        return true;
    }

    private void listDevices() {
        bluetoothDevices.clear();
        pairedDevices = mBluetoothUtil.getBondedDevices();

        for (BluetoothDevice device : pairedDevices) {
            bluetoothDevices.add(device);
        }

        mBluetoothDeviceAdapter.notifyDataSetChanged();
    }

    final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                bluetoothDevices.add(device);
                mBluetoothDeviceAdapter.notifyDataSetChanged();
            }
        }
    };

    class SensorsObserver extends ContentObserver {

        public SensorsObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
//            Cursor sensorCursor = getApplicationContext().getContentResolver()
//                    .query(SensorsProvider.CONTENT_URI, null,
//                            DbConstants.Sensor.KEY_ROWID + " = (SELECT MAX("
//                                    + DbConstants.Sensor.KEY_ROWID + ")  FROM "
//                                    + DbConstants.Sensor.DATABASE_TABLE + ")",
//                            null, null);
            Cursor sensorCursor = getApplicationContext().getContentResolver()
                    .query(SensorsProvider.CONTENT_URI, null, null, null, null);
            String values = "";
            if (sensorCursor.moveToFirst()) {
                while (!sensorCursor.isAfterLast()) {
                    Log.i("test", sensorCursor.getString(1));
                    values = values
                            .concat(sensorCursor.getString(1) + " " + sensorCursor.getString(2)
                                    + " "
                                    + sensorCursor.getString(3) + "\n");

                    sensorCursor.moveToNext();
                }
                lastEntry.setText(values);
            }
            sensorCursor.close();
        }
    }
}
