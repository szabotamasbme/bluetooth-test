package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage504 extends InventureMessage {

    private float fuelLevel1;

    private InventureMessage504(float fuelLevel1) {
        super();
        this.fuelLevel1 = fuelLevel1;
    }

    public InventureMessage504(String[] data) {
        this(Float.valueOf(data[0]));
    }

    public float getFuelLevel1() {
        return fuelLevel1;
    }

    @Override
    public String toString() {
        return "InventureMessage504{" +
                "fuelLevel1=" + fuelLevel1 +
                '}';
    }
}
