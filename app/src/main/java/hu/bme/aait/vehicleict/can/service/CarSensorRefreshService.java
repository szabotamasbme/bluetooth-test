package hu.bme.aait.vehicleict.can.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

import java.util.List;

import hu.bme.aait.vehicleict.can.bluetooth.BluetoothAdapterUtil;
import hu.bme.aait.vehicleict.can.bluetooth.BluetoothState;
import hu.bme.aait.vehicleict.can.inventure.CarSensor;
import hu.bme.aait.vehicleict.can.inventure.InventureMessageParser;
import hu.bme.aait.vehicleict.can.inventure.SensorData;
import hu.bme.aait.vehicleict.can.inventure.SensorDataUtil;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage;
import hu.bme.aait.vehicleict.can.provider.SensorsProvider;

public class CarSensorRefreshService extends Service {

    BluetoothAdapterUtil mBluetoothUtil;

    String remoteDeviceName;

    int mStartMode;

    private CarSensor carSensor;

    private SensorsProvider mSensorsProvider;

    public CarSensorRefreshService() {
    }

    @Override
    public void onCreate() {
        mBluetoothUtil = new BluetoothAdapterUtil(this);
        carSensor = new CarSensor();
        mSensorsProvider = new SensorsProvider();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle bundle = intent.getExtras();
        remoteDeviceName = bundle.getString("remoteDeviceName");

        if (mBluetoothUtil.isEnabled()) {
            if (!mBluetoothUtil.isServiceAvailable()) {
                mBluetoothUtil.setupService();
                mBluetoothUtil.startService(BluetoothState.DEVICE_OTHER);
            }

            mBluetoothUtil
                    .setOnDataReceivedListener(new BluetoothAdapterUtil.OnDataReceivedListener() {
                        @Override
                        public void onDataReceived(byte[] data, String message) {
                            InventureMessage inventureMessage = InventureMessageParser
                                    .parse(message);
//                            String inventureMessageString = inventureMessage.toString();
//                            Log.i("test", inventureMessageString);
                            carSensor = SensorDataUtil
                                    .refreshCarSensor(inventureMessage, carSensor);
                            List<SensorData> sensorDatas = SensorDataUtil
                                    .getSensorDataFromInventureMessage(inventureMessage);
                            for (SensorData sensorData : sensorDatas) {
                                mSensorsProvider.insert(sensorData, getApplicationContext());
                            }
                        }
                    });

            if (remoteDeviceName != null && !remoteDeviceName.equals("")) {
                mBluetoothUtil.autoConnect(remoteDeviceName);
            }
        } else {
            stopSelf();
        }

        return mStartMode;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBluetoothUtil.disconnect();
        mBluetoothUtil.stopAutoConnect();
        mBluetoothUtil.stopService();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}
