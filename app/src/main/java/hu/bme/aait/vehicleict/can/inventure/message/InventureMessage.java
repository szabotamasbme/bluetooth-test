package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public abstract class InventureMessage {

    public InventureMessage(String[] data) {
    }

    public InventureMessage() {

    }

    public int getMessageId() {
        return Integer.valueOf(this.getClass().getSimpleName().substring(this.getClass().getSimpleName().length() - 3));
    }
}
