package hu.bme.aait.vehicleict.can.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hu.bute.aait.gorog.bluetoothtest.R;

/**
 * Created by Gorog on 2014.10.12.
 */
public class SensorsFragment extends ListFragment {

    @InjectView(android.R.id.list)
    ListView listView;

    public static final String TAG = SensorsFragment.class.getName();

    SensorsActionListener mCallback;

    public interface SensorsActionListener {

        public void onSensorsFragmentStarted();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (SensorsActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_sensors, container,
                false);
        ButterKnife.inject(this, root);

        mCallback.onSensorsFragmentStarted();
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mCallback.onSensorsFragmentStarted();
    }
}
