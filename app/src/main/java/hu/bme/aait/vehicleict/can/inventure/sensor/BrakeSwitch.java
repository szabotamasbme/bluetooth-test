package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
 * Created by Gorog on 2014.10.12.
 */
public enum BrakeSwitch {
    PEDAL_RELEASED(0), PEDAL_DEPRESSED(1);

    private final int value;

    BrakeSwitch(final int value) {
        this.value = value;
    }

    public static BrakeSwitch fromValue(int value) {
        for (BrakeSwitch b : BrakeSwitch.values()) {
            if (b.value == value) {
                return b;
            }
        }
        return null;
    }
}
