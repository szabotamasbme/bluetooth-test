package hu.bme.aait.vehicleict.can.inventure.message;

import hu.bme.aait.vehicleict.can.inventure.sensor.BrakeSwitch;
import hu.bme.aait.vehicleict.can.inventure.sensor.ClutchSwitch;
import hu.bme.aait.vehicleict.can.inventure.sensor.CruiseControlActive;
import hu.bme.aait.vehicleict.can.inventure.sensor.PTOState;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage501 extends InventureMessage {

    private float wheelBasedSpeed;

    private ClutchSwitch clutchSwitch;

    private BrakeSwitch brakeSwitch;

    private CruiseControlActive cruiseControlActive;

    private PTOState ptoState;

    private InventureMessage501(float wheelBasedSpeed, int clutchSwitch, int brakeSwitch,
            int cruiseControlActive, int ptoState) {
        super();
        this.wheelBasedSpeed = wheelBasedSpeed;
        this.clutchSwitch = ClutchSwitch.fromValue(clutchSwitch);
        this.brakeSwitch = BrakeSwitch.fromValue(brakeSwitch);
        this.cruiseControlActive = CruiseControlActive.fromValue(cruiseControlActive);
        this.ptoState = PTOState.fromValue(ptoState);
    }

    public InventureMessage501(String[] data) {
        this(Float.valueOf(data[0]), Integer.valueOf(data[1]),
                Integer.valueOf(data[2]), Integer.valueOf(data[3]), Integer.valueOf(data[4]));
    }

    public float getWheelBasedSpeed() {
        return wheelBasedSpeed;
    }

    public ClutchSwitch getClutchSwitch() {
        return clutchSwitch;
    }

    public BrakeSwitch getBrakeSwitch() {
        return brakeSwitch;
    }

    public CruiseControlActive getCruiseControlActive() {
        return cruiseControlActive;
    }

    public PTOState getPTOState() {
        return ptoState;
    }

    @Override
    public String toString() {
        return "InventureMessage501{" +
                "wheelBasedSpeed=" + wheelBasedSpeed +
                ", clutchSwitch=" + clutchSwitch +
                ", brakeSwitch=" + brakeSwitch +
                ", cruiseControlActive=" + cruiseControlActive +
                ", ptoState=" + ptoState +
                '}';
    }


}
