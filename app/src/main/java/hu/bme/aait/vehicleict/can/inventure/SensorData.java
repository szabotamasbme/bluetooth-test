package hu.bme.aait.vehicleict.can.inventure;

import hu.bme.aait.vehicleict.can.inventure.sensor.SensorType;

/**
 * Created by Gorog on 2014.10.12.
 */
public class SensorData {

    private Object value;

    private long time;

    private SensorType sensorType;

    public SensorData(Object value, SensorType sensorType) {
        this.value = value;
        this.time = 0;
        this.sensorType = sensorType;
    }

    public SensorData(Object value, SensorType sensorType, long time) {
        this.value = value;
        this.time = time;
        this.sensorType = sensorType;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
        this.time = System.currentTimeMillis();
    }

    public void setValue(Object value, long time) {
        this.value = value;
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    public SensorType getSensorType() {
        return sensorType;
    }
}
