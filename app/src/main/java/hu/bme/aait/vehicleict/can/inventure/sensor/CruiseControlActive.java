package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum CruiseControlActive {
    SWITCHED_OFF(0), SWITCHED_ON(1);

    private final int value;

    CruiseControlActive(final int value) {
        this.value = value;
    }

    public static CruiseControlActive fromValue(int value) {
        for (CruiseControlActive cc : CruiseControlActive.values()) {
            if (cc.value == value) {
                return cc;
            }
        }
        return null;
    }
}
