package hu.bme.aait.vehicleict.can.inventure;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hu.bme.aait.vehicleict.can.activity.BluetoothActivity;
import hu.bute.aait.gorog.bluetoothtest.R;

/**
 * Created by Gorog on 2014.09.22.
 */
public class SensorDataAdapter extends BaseAdapter {

    @Override
    public int getCount() {
        return arrayListSensorDatas.size();
    }

    @Override
    public SensorData getItem(int iPosition) {
        return arrayListSensorDatas.get(iPosition);
    }

    @Override
    public long getItemId(int iID) {
        return 0;
    }

    LayoutInflater inflater;

    List<SensorData> arrayListSensorDatas = new ArrayList<SensorData>();

    static Context context;

    BluetoothActivity activity;

    public SensorDataAdapter(Context context, List<SensorData> sensorDatas,
                             BluetoothActivity activity) {
        arrayListSensorDatas = sensorDatas;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
        this.activity = activity;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.listitem_sensordata, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.name.setText(arrayListSensorDatas.get(position).getSensorType().getNameId());
        if (arrayListSensorDatas.get(position).getSensorType().getChartType() == ChartType.Time
                || arrayListSensorDatas.get(position).getSensorType().getChartType() == ChartType.List) {
            holder.drawButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    activity.selectChart(
                            arrayListSensorDatas.get(position)
                                    .getSensorType());
                }
            });
        } else {
            holder.drawButton.setEnabled(false);
        }

        return view;
    }

    static class ViewHolder {

        @InjectView(R.id.sensorName)
        TextView name;

        @InjectView(R.id.drawButton)
        TextView drawButton;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
