package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage512 extends InventureMessage {

    private int engineCoolantTemperature;

    private InventureMessage512(int engineCoolantTemperature) {
        super();
        this.engineCoolantTemperature = engineCoolantTemperature;
    }

    public InventureMessage512(String[] data) {
        this(Integer.valueOf(data[0]));
    }

    public int getEngineCoolantTemperature() {
        return engineCoolantTemperature;
    }

    @Override
    public String toString() {
        return "InventureMessage512{" +
                "engineCoolantTemperature=" + engineCoolantTemperature +
                '}';
    }
}
