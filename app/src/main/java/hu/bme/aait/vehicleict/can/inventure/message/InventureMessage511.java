package hu.bme.aait.vehicleict.can.inventure.message;

import hu.bme.aait.vehicleict.can.inventure.sensor.DirectionIndicator;
import hu.bme.aait.vehicleict.can.inventure.sensor.DriverCard;
import hu.bme.aait.vehicleict.can.inventure.sensor.DriverTimeRelativeStates;
import hu.bme.aait.vehicleict.can.inventure.sensor.DriverWorkingStates;
import hu.bme.aait.vehicleict.can.inventure.sensor.HandlingInformation;
import hu.bme.aait.vehicleict.can.inventure.sensor.SystemEvent;
import hu.bme.aait.vehicleict.can.inventure.sensor.TachgraphPerformance;
import hu.bme.aait.vehicleict.can.inventure.sensor.VehicleMotion;
import hu.bme.aait.vehicleict.can.inventure.sensor.VehicleOverSpeed;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage511 extends InventureMessage {

    private VehicleMotion vehicleMotion;

    private VehicleOverSpeed vehicleOverspeed;

    private DirectionIndicator directionIndicator;

    private DriverWorkingStates driv1WorkingStates;

    private DriverWorkingStates driv2WorkingStates;

    private DriverCard driver1Card;

    private DriverCard driver2Card;

    private DriverTimeRelativeStates driver1TimeRelativeStates;

    private DriverTimeRelativeStates driver2TimeRelativeStates;

    private TachgraphPerformance tachgraphPerformance;

    private HandlingInformation handlingInformation;

    private SystemEvent systemEvent;

    private float tachogrVehicleSpeed;

    private InventureMessage511(int vehicleMotion, int vehicleOverspeed, int directionIndicator,
            int driv1WorkingStates, int driv2WorkingStates, int driver1Card, int driver2Card,
            int driver1TimeRelativeStates, int driver2TimeRelativeStates, int tachgraphPerformance,
            int handlingInformation, int systemEvent, float tachogrVehicleSpeed) {
        this.vehicleMotion = VehicleMotion.fromValue(vehicleMotion);
        this.vehicleOverspeed = VehicleOverSpeed.fromValue(vehicleOverspeed);
        this.directionIndicator = DirectionIndicator.fromValue(directionIndicator);
        this.driv1WorkingStates = DriverWorkingStates.fromValue(driv1WorkingStates);
        this.driv2WorkingStates = DriverWorkingStates.fromValue(driv2WorkingStates);
        this.driver1Card = DriverCard.fromValue(driver1Card);
        this.driver2Card = DriverCard.fromValue(driver2Card);
        this.driver1TimeRelativeStates = DriverTimeRelativeStates
                .fromValue(driver1TimeRelativeStates);
        this.driver2TimeRelativeStates = DriverTimeRelativeStates
                .fromValue(driver2TimeRelativeStates);
        this.tachgraphPerformance = TachgraphPerformance.fromValue(tachgraphPerformance);
        this.handlingInformation = HandlingInformation.fromValue(handlingInformation);
        this.systemEvent = SystemEvent.fromValue(systemEvent);
        this.tachogrVehicleSpeed = tachogrVehicleSpeed;
    }

    public InventureMessage511(String[] data) {
        this(Integer.valueOf(data[0]), Integer.valueOf(data[1]), Integer.valueOf(data[2]),
                Integer.valueOf(data[3]), Integer.valueOf(data[4]), Integer.valueOf(data[5]),
                Integer.valueOf(data[6]), Integer.valueOf(data[7]), Integer.valueOf(data[8]),
                Integer.valueOf(data[9]), Integer.valueOf(data[10]), Integer.valueOf(data[11]),
                Float.valueOf(data[12]));
    }

    public VehicleMotion getVehicleMotion() {
        return vehicleMotion;
    }

    public VehicleOverSpeed getVehicleOverspeed() {
        return vehicleOverspeed;
    }

    public DirectionIndicator getDirectionIndicator() {
        return directionIndicator;
    }

    public DriverWorkingStates getDriv1WorkingStates() {
        return driv1WorkingStates;
    }

    public DriverWorkingStates getDriv2WorkingStates() {
        return driv2WorkingStates;
    }

    public DriverCard getDriver1Card() {
        return driver1Card;
    }

    public DriverCard getDriver2Card() {
        return driver2Card;
    }

    public DriverTimeRelativeStates getDriver1TimeRelativeStates() {
        return driver1TimeRelativeStates;
    }

    public DriverTimeRelativeStates getDriver2TimeRelativeStates() {
        return driver2TimeRelativeStates;
    }

    public TachgraphPerformance getTachgraphPerformance() {
        return tachgraphPerformance;
    }

    public HandlingInformation getHandlingInformation() {
        return handlingInformation;
    }

    public SystemEvent getSystemEvent() {
        return systemEvent;
    }

    public float getTachogrVehicleSpeed() {
        return tachogrVehicleSpeed;
    }

    @Override
    public String toString() {
        return "InventureMessage511{" +
                "vehicleMotion=" + vehicleMotion +
                ", vehicleOverspeed=" + vehicleOverspeed +
                ", directionIndicator=" + directionIndicator +
                ", driv1WorkingStates=" + driv1WorkingStates +
                ", driv2WorkingStates=" + driv2WorkingStates +
                ", driver1Card=" + driver1Card +
                ", driver2Card=" + driver2Card +
                ", driver1TimeRelativeStates=" + driver1TimeRelativeStates +
                ", driver2TimeRelativeStates=" + driver2TimeRelativeStates +
                ", tachgraphPerformance=" + tachgraphPerformance +
                ", handlingInformation=" + handlingInformation +
                ", systemEvent=" + systemEvent +
                ", tachogrVehicleSpeed=" + tachogrVehicleSpeed +
                '}';
    }

}