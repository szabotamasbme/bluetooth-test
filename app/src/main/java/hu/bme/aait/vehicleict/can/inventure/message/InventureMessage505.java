package hu.bme.aait.vehicleict.can.inventure.message;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessage505 extends InventureMessage {

    private float engineSpeed;

    private InventureMessage505(float engineSpeed) {
        super();
        this.engineSpeed = engineSpeed;
    }

    public InventureMessage505(String[] data) {
        this(Float.valueOf(data[0]));
    }

    public float getEngineSpeed() {
        return engineSpeed;
    }

    @Override
    public String toString() {
        return "InventureMessage505{" +
                "engineSpeed=" + engineSpeed +
                '}';
    }
}
