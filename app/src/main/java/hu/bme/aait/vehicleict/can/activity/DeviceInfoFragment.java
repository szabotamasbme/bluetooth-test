package hu.bme.aait.vehicleict.can.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemLongClick;
import hu.bute.aait.gorog.bluetoothtest.R;

/**
 * Created by Gorog on 2014.10.12.
 */
public class DeviceInfoFragment extends Fragment {

    public static final String TAG = DeviceInfoFragment.class.getName();


    @InjectView(R.id.onButton)
    Button onButton;

    @InjectView(R.id.offButton)
    Button offButton;

    @InjectView(R.id.listButton)
    Button listButton;

    @InjectView(R.id.statusText)
    TextView statusText;

    @InjectView(R.id.savedDeviceAddress)
    TextView savedDeviceAddress;

    @InjectView(R.id.deviceListView)
    ListView deviceListView;

    DeviceInfoActionListener mCallback;

    public interface DeviceInfoActionListener {

        public void onDeviceTurnOn();

        public void onDeviceTurnOff();

        public void onDeviceInfoFragmentStarted();

        void onListDevices();

        void onSearchDevices();

        boolean onDeviceSelected(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (DeviceInfoActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_bluetooth, container,
                false);
        ButterKnife.inject(this, root);

        mCallback.onDeviceInfoFragmentStarted();
        return root;
    }

    @OnClick(R.id.onButton)
    public void turnOn() {
        mCallback.onDeviceTurnOn();
    }

    @OnClick(R.id.offButton)
    public void turnOff() {
        mCallback.onDeviceTurnOff();
    }

    @OnClick(R.id.listButton)
    public void listDevices() {
        mCallback.onListDevices();
    }

    @OnClick(R.id.searchButton)
    public void searchDevices() {
        mCallback.onSearchDevices();
    }

    @OnItemLongClick(R.id.deviceListView)
    public boolean deviceSelected(int position) {
        return mCallback.onDeviceSelected(position);
    }

    public void setButtonsEnabled() {
        onButton.setEnabled(true);
        offButton.setEnabled(true);
        listButton.setEnabled(true);
        statusText.setText("Status: Enabled");
    }

    public void setButtonsDisabled() {
        onButton.setEnabled(false);
        offButton.setEnabled(false);
        listButton.setEnabled(false);
        statusText.setText("Status: Disabled");
    }

    public void setSavedDeviceAddress(String remoteDeviceName, String remoteDeviceAddress) {
        savedDeviceAddress.setText(getString(R.string.saved_remote_device) + ": " +
                remoteDeviceName + " (" + remoteDeviceAddress + ")");
    }
}
