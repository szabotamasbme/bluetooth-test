package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum HandlingInformation {
    NO_HANDLING_INFORMATION(0), HANDLING_INFORMATION(1);

    private final int value;

    HandlingInformation(final int value) {
        this.value = value;
    }

    public static HandlingInformation fromValue(int value) {
        for (HandlingInformation t : HandlingInformation.values()) {
            if (t.value == value) {
                return t;
            }
        }
        return null;
    }
}
