package hu.bme.aait.vehicleict.can.util;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hu.bute.aait.gorog.bluetoothtest.R;

/**
 * Created by Gorog on 2014.09.22.
 */
public class BluetoothDeviceAdapter extends BaseAdapter {

    @Override
    public int getCount() {
        return arrayListBluetoothDevices.size();
    }

    @Override
    public BluetoothDevice getItem(int iPosition) {
        return arrayListBluetoothDevices.get(iPosition);
    }

    @Override
    public long getItemId(int iID) {
        return 0;
    }

    LayoutInflater inflater;

    ArrayList<BluetoothDevice> arrayListBluetoothDevices = new ArrayList<BluetoothDevice>();

    static Context context;

    Activity activity;

    public BluetoothDeviceAdapter(Context context, ArrayList<BluetoothDevice> bluetoothDevices,
            Activity activity) {
        arrayListBluetoothDevices = bluetoothDevices;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.listitem_bluetoothdevice, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.name.setText(arrayListBluetoothDevices.get(position).getName());
        holder.address.setText(arrayListBluetoothDevices.get(position).getAddress());

        return view;
    }

    static class ViewHolder {

        @InjectView(R.id.deviceName)
        TextView name;

        @InjectView(R.id.address)
        TextView address;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
