package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum VehicleMotion {
    VEHICLE_MOTION_NOT_DETECTED(0), VEHICLE_MOTION_DETECTED(1);

    private final int value;

    VehicleMotion(final int value) {
        this.value = value;
    }

    public static VehicleMotion fromValue(int value) {
        for (VehicleMotion v : VehicleMotion.values()) {
            if (v.value == value) {
                return v;
            }
        }
        return null;
    }
}
