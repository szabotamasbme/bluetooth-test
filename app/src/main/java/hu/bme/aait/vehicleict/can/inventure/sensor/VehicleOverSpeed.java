package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum VehicleOverSpeed {
    NO_OVERSPEED(0), OVERSPEED(1);

    private final int value;

    VehicleOverSpeed(final int value) {
        this.value = value;
    }

    public static VehicleOverSpeed fromValue(int value) {
        for (VehicleOverSpeed v : VehicleOverSpeed.values()) {
            if (v.value == value) {
                return v;
            }
        }
        return null;
    }
}
