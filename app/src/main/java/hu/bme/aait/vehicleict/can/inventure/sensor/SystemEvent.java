package hu.bme.aait.vehicleict.can.inventure.sensor;

/**
* Created by Gorog on 2014.10.12.
*/
public enum SystemEvent {
    NO_TACHOGRAPH_EVENT(0), TACHOGRAPH_EVENT(1);

    private final int value;

    SystemEvent(final int value) {
        this.value = value;
    }

    public static SystemEvent fromValue(int value) {
        for (SystemEvent t : SystemEvent.values()) {
            if (t.value == value) {
                return t;
            }
        }
        return null;
    }
}
