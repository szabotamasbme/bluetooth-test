package hu.bme.aait.vehicleict.can.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import java.util.HashMap;

import hu.bme.aait.vehicleict.can.db.DatabaseHelper;
import hu.bme.aait.vehicleict.can.db.DbConstants;
import hu.bme.aait.vehicleict.can.inventure.SensorData;

/**
 * Created by Patrik on 2014.10.26..
 */
public class SensorsProvider extends ContentProvider {

    static final String PROVIDER_NAME = "hu.bme.aait.vehicleict.can.provider.SensorsProvider";

    static final String URL = "content://" + PROVIDER_NAME + "/"
            + DbConstants.Sensor.DATABASE_TABLE;

    public static final Uri CONTENT_URI = Uri.parse(URL);

    static final UriMatcher uriMatcher;

    static final int SENSORS = 1;

    static final int SENSOR_ID = 2;

    private static HashMap<String, String> sensorsProjectionMap;

    private DatabaseHelper dbHelper;

    //statikus adatok inicializálása
    static {
        uriMatcher = new UriMatcher(
                UriMatcher.NO_MATCH);                                           //uriMatcher
        uriMatcher.addURI(PROVIDER_NAME, DbConstants.Sensor.DATABASE_TABLE, SENSORS);
        uriMatcher.addURI(PROVIDER_NAME, DbConstants.Sensor.DATABASE_TABLE + "/#", SENSOR_ID);

        sensorsProjectionMap
                = new HashMap<String, String>();                                       //oszlopok
        sensorsProjectionMap.put(DbConstants.Sensor.KEY_ROWID, DbConstants.Sensor.KEY_ROWID);
        sensorsProjectionMap.put(DbConstants.Sensor.KEY_TYPE, DbConstants.Sensor.KEY_TYPE);
        sensorsProjectionMap.put(DbConstants.Sensor.KEY_VALUE, DbConstants.Sensor.KEY_VALUE);
        sensorsProjectionMap.put(DbConstants.Sensor.KEY_TIME, DbConstants.Sensor.KEY_TIME);
    }


    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        Context context = getContext();
        dbHelper = new DatabaseHelper(context);

        db = dbHelper.getWritableDatabase();
        db.delete(DbConstants.Sensor.DATABASE_TABLE, null, null);
        return (db == null);

    }

    /**
     * Insert the given content values
     *
     * @param uri           Table uri.
     * @param contentValues this map contains the initial column values for the row. The keys should
     *                      be the column names and the values the column values
     * @return Uri of the created row.
     */
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {

        long rowID = (int) db
                .insertWithOnConflict(DbConstants.Sensor.DATABASE_TABLE, "", contentValues,
                        SQLiteDatabase.CONFLICT_IGNORE);

        if (rowID == -1) {
            rowID = (int) db
                    .update(DbConstants.Sensor.DATABASE_TABLE, contentValues,
                            DbConstants.Sensor.KEY_TYPE + "=?",
                            new String[]{contentValues.getAsString(DbConstants.Sensor.KEY_TYPE)});
        }

        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }

        return null;
    }


    /**
     * @param uri           the table to update in
     * @param contentValues a map from column names to new column values. null is a valid value that
     *                      will be translated to NULL.
     * @param where         the optional WHERE clause to apply when updating. Passing null will
     *                      update all rows.
     * @param whereArgs     You may include ?s in the where clause, which will be replaced by the
     *                      values from whereArgs. The values will be bound as Strings.
     * @return The number of rows affected
     */
    @Override
    public int update(Uri uri, ContentValues contentValues, String where, String[] whereArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case SENSORS:
                count = db
                        .update(DbConstants.Sensor.DATABASE_TABLE, contentValues, where, whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    /**
     * Delete rows that match the given conditions
     *
     * @param uri       Table uri.
     * @param where     Optional WHERE clause to apply when deleting. Passing null will delete all
     *                  rows.
     * @param whereArgs You may include ?s in the where clause, which will be replaced by the values
     *                  from whereArgs. The values will be bound as Strings.
     * @return Number of rows deleted.
     */
    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        switch (uriMatcher.match(uri)) {
            case SENSORS:
                break;
            case SENSOR_ID:
                where = where + "_id = " + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        int count = db.delete(DbConstants.Sensor.DATABASE_TABLE, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    /**
     * Creates a query
     *
     * @param uri           Table uri
     * @param projection    columns (null for all columns)
     * @param selection     select clause (null for no clause)
     * @param selectionArgs select arguments (null for no arguments)
     * @param sortOrder     sort order (null for default sort order)
     * @return Cursor
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(DbConstants.Sensor.DATABASE_TABLE);
        qb.setProjectionMap(sensorsProjectionMap);

        int match = uriMatcher.match(uri);
        switch (match) {
            case SENSORS:
                break;
            case SENSOR_ID:
                selection = selection + "_id = " + uri.getLastPathSegment();
                break;
            case UriMatcher.NO_MATCH:
                Log.d(this.getClass().toString(), "Uri NO_MATCH in query");
                throw new IllegalArgumentException("Unknown URI " + uri);
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (sortOrder == null || sortOrder.equals("")) {
            sortOrder = DbConstants.Sensor.KEY_ROWID;
        }
        Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            /**
             * Get all sensor records
             */
            case SENSORS:
                return "vnd.android.cursor.dir/vnd";
            /**
             * Get a particular sensor
             */
            case SENSOR_ID:
                return "vnd.android.cursor.item/vnd";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    /**
     * Delete all rows
     *
     * @param c Context where the method was called from
     * @return int number of rows deleted
     */
    public int deleteAll(Context c) {
        return c.getContentResolver().delete(CONTENT_URI, null, null);
    }


    /**
     * Insert SensorData
     *
     * @param sd SensorData that should be inserted
     * @param c  Context where the method was called from
     * @return Uri of the created row
     */
    public Uri insert(SensorData sd, Context c) {
        ContentValues values = new ContentValues();
        String s[] = {sd.getSensorType().toString(), sd.getValue().toString()};

        values.put(DbConstants.Sensor.KEY_TYPE, s[0]);
        values.put(DbConstants.Sensor.KEY_VALUE, s[1]);
        values.put(DbConstants.Sensor.KEY_TIME, sd.getTime());

        return c.getContentResolver().insert(
                SensorsProvider.CONTENT_URI, values);
    }
}
