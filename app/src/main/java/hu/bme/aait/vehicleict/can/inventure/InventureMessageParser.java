package hu.bme.aait.vehicleict.can.inventure;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage501;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage502;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage503;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage504;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage505;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage506;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage507;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage508;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage509;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage510;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage511;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage512;

/**
 * Created by Gorog on 2014.09.30.
 */
public class InventureMessageParser {

    private static final String patternString
            = "^\\$PINV(\\d\\d\\d),([^*]+)*\\*([A-F0-9][A-F0-9])$";


    private static String values;

    private static String checksum;

    public static InventureMessage parse(String message) {
        InventureMessage inventureMessage = null;

        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(message);

        while (matcher.find()) {
            int messageIdentifier = Integer.valueOf(matcher.group(1));
            values = matcher.group(2);
            String[] data = values.split(",");
            switch (messageIdentifier) {
                case 501:
                    inventureMessage = new InventureMessage501(data);
                    break;
                case 502:
                    inventureMessage = new InventureMessage502(data);
                    break;
                case 503:
                    inventureMessage = new InventureMessage503(data);
                    break;
                case 504:
                    inventureMessage = new InventureMessage504(data);
                    break;
                case 505:
                    inventureMessage = new InventureMessage505(data);
                    break;
                case 506:
                    inventureMessage = new InventureMessage506(data);
                    break;
                case 507:
                    inventureMessage = new InventureMessage507(data);
                    break;
                case 508:
                    inventureMessage = new InventureMessage508(data);
                    break;
                case 509:
                    inventureMessage = new InventureMessage509(data);
                    break;
                case 510:
                    inventureMessage = new InventureMessage510(data);
                    break;
                case 511:
                    inventureMessage = new InventureMessage511(data);
                    break;
                case 512:
                    inventureMessage = new InventureMessage512(data);
                    break;
            }

            checksum = matcher.group(3);
        }
        return inventureMessage;
    }
}
