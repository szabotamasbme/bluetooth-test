package hu.bme.aait.vehicleict.can.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hu.bme.aait.vehicleict.can.inventure.ChartType;
import hu.bme.aait.vehicleict.can.inventure.SensorData;
import hu.bute.aait.gorog.bluetoothtest.R;

/**
 * Created by Gorog on 2014.10.12.
 */
public class VisualisationFragment extends Fragment {

    public static final String TAG = VisualisationFragment.class.getName();

    @InjectView(R.id.chart)
    LinearLayout chart;

    @InjectView(R.id.chartName)
    TextView chartName;

    private List<SensorData> sensorDataList;

    private ChartType chartType = ChartType.Time;

    ChartActionListener mCallback;

    private GraphicalView chartView;

    public interface ChartActionListener {

        public void onChartFragmentStarted();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (ChartActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_chart, container,
                false);
        ButterKnife.inject(this, root);

        mCallback.onChartFragmentStarted();
        return root;
    }

    public void setSensorDataList(List<SensorData> sensorDataList) {
        this.sensorDataList = sensorDataList;
    }

    public void refreshChart() {
        if (!sensorDataList.isEmpty()) {
            chartName.setText(getResources().getString(sensorDataList.get(0).getSensorType().getNameId()));
            switch (sensorDataList.get(0).getSensorType().getChartType()) {
                case Time:
                    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();

                    XYSeries series = new XYSeries(
                            getResources().getString(sensorDataList.get(0).getSensorType().getNameId()));
                    // Now we create the renderer
                    XYSeriesRenderer renderer = new XYSeriesRenderer();
                    renderer.setLineWidth(2);
                    renderer.setColor(Color.RED);
                    // Include low and max value
                    renderer.setDisplayBoundingPoints(true);
                    // we add point markers
                    renderer.setPointStyle(PointStyle.CIRCLE);
                    renderer.setPointStrokeWidth(3);

                    for (SensorData s : sensorDataList) {
                        if (s != null) {
//                        Log.i(getActivity().getString(s.getNameId()),
//                                String.valueOf(s.getTime()) + " " + String.valueOf(s.getValue()));
                            float value = 0f;
                            if (s.getValue() instanceof Float) {
                                value = (Float) s.getValue();
                            } else if (s.getValue() instanceof Integer) {
                                value = (float) (Integer) s.getValue();
                            }
                            series.add(s.getTime(), value);
                        }
                    }

                    dataset.addSeries(series);

                    XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
                    mRenderer.addSeriesRenderer(renderer);
                    // We want to avoid black border
                    mRenderer
                            .setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins
                    // Disable Pan on two axis
                    mRenderer.setPanEnabled(false, false);
                    mRenderer.setYAxisMax(getMax());
                    mRenderer.setYAxisMin(getMin());
                    mRenderer.setShowGrid(true); // we show the grid

                    if (chart != null) {
                        chart.removeView(chartView);
                    }

                    chartView = ChartFactory
                            .getTimeChartView(getActivity(), dataset, mRenderer, "HH:mm:ss");

                    chart.addView(chartView);
                    break;
                case List:
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
                    Calendar calendar = new GregorianCalendar();
                    TextView textView = new TextView(getActivity());
                    String results = "";
                    List<SensorData> newSensorDataList = new ArrayList<SensorData>(sensorDataList);
                    Collections.reverse(newSensorDataList);
                    for (SensorData s : newSensorDataList) {
                        if (s != null) {
                            calendar.setTimeInMillis(s.getTime());
                            simpleDateFormat.format(calendar.getTime());
                            results += simpleDateFormat.format(calendar.getTime()) + " " + s.getValue() + "\n";
                        }
                    }
                    textView.setText(results);
                    chart.removeAllViews();
                    chart.addView(textView);
                    break;
            }
        }
    }

    private Float getMax() {
        float max = 0;
        for (SensorData s : sensorDataList) {
            if (s != null) {
                float value = 0f;
                if (s.getValue() instanceof Float) {
                    value = (Float) s.getValue();
                } else if (s.getValue() instanceof Integer) {
                    value = (float) (Integer) s.getValue();
                }
                if (value > max) {
                    max = value;
                }
            }
        }
        return max;
    }

    private Float getMin() {
        float min = Float.MAX_VALUE;
        for (SensorData s : sensorDataList) {
            if (s != null) {
                float value = 0f;
                if (s.getValue() instanceof Float) {
                    value = (Float) s.getValue();
                } else if (s.getValue() instanceof Integer) {
                    value = (float) (Integer) s.getValue();
                }
                if (value < min) {
                    min = value;
                }
            }
        }
        return min > 0 ? 0 : min;
    }
}
