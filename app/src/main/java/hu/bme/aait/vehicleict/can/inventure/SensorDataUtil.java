package hu.bme.aait.vehicleict.can.inventure;

import java.util.ArrayList;
import java.util.List;

import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage501;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage502;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage503;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage504;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage505;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage506;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage507;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage508;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage509;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage510;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage511;
import hu.bme.aait.vehicleict.can.inventure.message.InventureMessage512;
import hu.bme.aait.vehicleict.can.inventure.sensor.SensorType;

/**
 * Created by Gorog on 2014.10.19.
 */
public class SensorDataUtil {

    public static CarSensor refreshCarSensor(InventureMessage inventureMessage,
            CarSensor carSensor) {
        long time = System.currentTimeMillis();

        if (inventureMessage instanceof InventureMessage501) {
            carSensor.setSensorDataBySensorType(SensorType.WheelBasedSpeed,
                    ((InventureMessage501) inventureMessage).getWheelBasedSpeed(), time);
            carSensor.setSensorDataBySensorType(SensorType.BrakeSwitch,
                    ((InventureMessage501) inventureMessage).getBrakeSwitch(), time);
            carSensor.setSensorDataBySensorType(SensorType.ClutchSwitch,
                    ((InventureMessage501) inventureMessage).getClutchSwitch(), time);
            carSensor.setSensorDataBySensorType(SensorType.CruiseControlActive,
                    ((InventureMessage501) inventureMessage).getCruiseControlActive(), time);
            carSensor.setSensorDataBySensorType(SensorType.PTOState,
                    ((InventureMessage501) inventureMessage).getPTOState(), time);
        } else if (inventureMessage instanceof InventureMessage502) {
            carSensor.setSensorDataBySensorType(SensorType.AcceleratorPedalPosition1,
                    ((InventureMessage502) inventureMessage).getAcceleratorPedalPosition1(), time);
        } else if (inventureMessage instanceof InventureMessage503) {
            carSensor.setSensorDataBySensorType(SensorType.EngineTotalFuelUsed,
                    ((InventureMessage503) inventureMessage).getEngineTotalFuelUsed(), time);
        } else if (inventureMessage instanceof InventureMessage504) {
            carSensor.setSensorDataBySensorType(SensorType.FuelLevel1,
                    ((InventureMessage504) inventureMessage).getFuelLevel1(), time);
        } else if (inventureMessage instanceof InventureMessage505) {
            carSensor.setSensorDataBySensorType(SensorType.EngineSpeed,
                    ((InventureMessage505) inventureMessage).getEngineSpeed(), time);
        } else if (inventureMessage instanceof InventureMessage506) {
            carSensor.setSensorDataBySensorType(SensorType.AxleWeight1,
                    ((InventureMessage506) inventureMessage).getAxleWeight1(), time);
            carSensor.setSensorDataBySensorType(SensorType.AxleWeight2,
                    ((InventureMessage506) inventureMessage).getAxleWeight2(), time);
            carSensor.setSensorDataBySensorType(SensorType.AxleWeight3,
                    ((InventureMessage506) inventureMessage).getAxleWeight3(), time);
            carSensor.setSensorDataBySensorType(SensorType.AxleWeight4,
                    ((InventureMessage506) inventureMessage).getAxleWeight4(), time);
        } else if (inventureMessage instanceof InventureMessage507) {
            carSensor.setSensorDataBySensorType(SensorType.EngineTotalHoursOfOperation,
                    ((InventureMessage507) inventureMessage).getEngineTotalHoursOfOperation(),
                    time);
        } else if (inventureMessage instanceof InventureMessage508) {
            carSensor.setSensorDataBySensorType(SensorType.VehicleIdentificationNumber,
                    ((InventureMessage508) inventureMessage).getVehicleIdentificationNumber(),
                    time);
        } else if (inventureMessage instanceof InventureMessage509) {
            carSensor.setSensorDataBySensorType(SensorType.HighResolutionTotalVehicleDistance,
                    ((InventureMessage509) inventureMessage)
                            .getHighResolutionTotalVehicleDistance(), time);
        } else if (inventureMessage instanceof InventureMessage510) {
            carSensor.setSensorDataBySensorType(SensorType.ServiceDistance,
                    ((InventureMessage510) inventureMessage).getServiceDistance(), time);
        } else if (inventureMessage instanceof InventureMessage511) {
            carSensor.setSensorDataBySensorType(SensorType.VehicleMotion,
                    ((InventureMessage511) inventureMessage).getVehicleMotion(), time);
            carSensor.setSensorDataBySensorType(SensorType.VehicleOverspeed,
                    ((InventureMessage511) inventureMessage).getVehicleOverspeed(), time);
            carSensor.setSensorDataBySensorType(SensorType.DirectionIndicator,
                    ((InventureMessage511) inventureMessage).getDirectionIndicator(), time);
            carSensor.setSensorDataBySensorType(SensorType.Driv1WorkingStates,
                    ((InventureMessage511) inventureMessage).getDriv1WorkingStates(), time);
            carSensor.setSensorDataBySensorType(SensorType.Driv2WorkingStates,
                    ((InventureMessage511) inventureMessage).getDriv2WorkingStates(), time);
            carSensor.setSensorDataBySensorType(SensorType.Driver1Card,
                    ((InventureMessage511) inventureMessage).getDriver1Card(), time);
            carSensor.setSensorDataBySensorType(SensorType.Driver2Card,
                    ((InventureMessage511) inventureMessage).getDriver2Card(), time);
            carSensor.setSensorDataBySensorType(SensorType.Driver1TimeRelativeStates,
                    ((InventureMessage511) inventureMessage).getDriver1TimeRelativeStates(), time);
            carSensor.setSensorDataBySensorType(SensorType.Driver2TimeRelativeStates,
                    ((InventureMessage511) inventureMessage).getDriver2TimeRelativeStates(), time);
            carSensor.setSensorDataBySensorType(SensorType.TachgraphPerformance,
                    ((InventureMessage511) inventureMessage).getTachgraphPerformance(), time);
            carSensor.setSensorDataBySensorType(SensorType.HandlingInformation,
                    ((InventureMessage511) inventureMessage).getHandlingInformation(), time);
            carSensor.setSensorDataBySensorType(SensorType.SystemEvent,
                    ((InventureMessage511) inventureMessage).getSystemEvent(), time);
            carSensor.setSensorDataBySensorType(SensorType.TachogrVehicleSpeed,
                    ((InventureMessage511) inventureMessage).getTachogrVehicleSpeed(), time);
        } else if (inventureMessage instanceof InventureMessage512) {
            carSensor.setSensorDataBySensorType(SensorType.EngineCoolantTemperature,
                    ((InventureMessage512) inventureMessage).getEngineCoolantTemperature(), time);
        }
        return carSensor;
    }

    public static List<SensorData> getSensorDataFromInventureMessage(
            InventureMessage inventureMessage) {
        List<SensorData> sensorDatas = new ArrayList<SensorData>();
        long time = System.currentTimeMillis();

        if (inventureMessage instanceof InventureMessage501) {
            sensorDatas.add(new SensorData(
                    ((InventureMessage501) inventureMessage).getWheelBasedSpeed(),
                    SensorType.WheelBasedSpeed, time));
            sensorDatas
                    .add(new SensorData(((InventureMessage501) inventureMessage).getBrakeSwitch(),
                            SensorType.BrakeSwitch, time));
            sensorDatas
                    .add(new SensorData(((InventureMessage501) inventureMessage).getClutchSwitch(),
                            SensorType.ClutchSwitch, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage501) inventureMessage).getCruiseControlActive(),
                    SensorType.CruiseControlActive, time));
            sensorDatas.add(new SensorData(((InventureMessage501) inventureMessage).getPTOState(),
                    SensorType.PTOState, time));
        } else if (inventureMessage instanceof InventureMessage502) {
            sensorDatas.add(new SensorData(
                    ((InventureMessage502) inventureMessage).getAcceleratorPedalPosition1(),
                    SensorType.AcceleratorPedalPosition1, time));
        } else if (inventureMessage instanceof InventureMessage503) {
            sensorDatas.add(new SensorData(
                    ((InventureMessage503) inventureMessage).getEngineTotalFuelUsed(),
                    SensorType.EngineTotalFuelUsed, time));
        } else if (inventureMessage instanceof InventureMessage504) {
            sensorDatas.add(new SensorData(((InventureMessage504) inventureMessage).getFuelLevel1(),
                    SensorType.FuelLevel1, time));
        } else if (inventureMessage instanceof InventureMessage505) {
            sensorDatas
                    .add(new SensorData(((InventureMessage505) inventureMessage).getEngineSpeed(),
                            SensorType.EngineSpeed, time));
        } else if (inventureMessage instanceof InventureMessage506) {
            sensorDatas
                    .add(new SensorData(((InventureMessage506) inventureMessage).getAxleWeight1(),
                            SensorType.AxleWeight1, time));
            sensorDatas
                    .add(new SensorData(((InventureMessage506) inventureMessage).getAxleWeight2(),
                            SensorType.AxleWeight2, time));
            sensorDatas
                    .add(new SensorData(((InventureMessage506) inventureMessage).getAxleWeight3(),
                            SensorType.AxleWeight3, time));
            sensorDatas
                    .add(new SensorData(((InventureMessage506) inventureMessage).getAxleWeight4(),
                            SensorType.AxleWeight4, time));
        } else if (inventureMessage instanceof InventureMessage507) {
            sensorDatas.add(new SensorData(
                    ((InventureMessage507) inventureMessage).getEngineTotalHoursOfOperation(),
                    SensorType.EngineTotalHoursOfOperation, time));
        } else if (inventureMessage instanceof InventureMessage508) {
            sensorDatas.add(new SensorData(
                    ((InventureMessage508) inventureMessage).getVehicleIdentificationNumber(),
                    SensorType.VehicleIdentificationNumber, time));
        } else if (inventureMessage instanceof InventureMessage509) {
            sensorDatas.add(new SensorData(((InventureMessage509) inventureMessage)
                    .getHighResolutionTotalVehicleDistance(),
                    SensorType.HighResolutionTotalVehicleDistance, time));
        } else if (inventureMessage instanceof InventureMessage510) {
            sensorDatas.add(new SensorData(
                    ((InventureMessage510) inventureMessage).getServiceDistance(),
                    SensorType.ServiceDistance, time));
        } else if (inventureMessage instanceof InventureMessage511) {
            sensorDatas
                    .add(new SensorData(((InventureMessage511) inventureMessage).getVehicleMotion(),
                            SensorType.VehicleMotion, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage511) inventureMessage).getVehicleOverspeed(),
                    SensorType.VehicleOverspeed, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage511) inventureMessage).getDirectionIndicator(),
                    SensorType.DirectionIndicator, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage511) inventureMessage).getDriv1WorkingStates(),
                    SensorType.Driv1WorkingStates, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage511) inventureMessage).getDriv2WorkingStates(),
                    SensorType.Driv2WorkingStates, time));
            sensorDatas
                    .add(new SensorData(((InventureMessage511) inventureMessage).getDriver1Card(),
                            SensorType.Driver1Card, time));
            sensorDatas
                    .add(new SensorData(((InventureMessage511) inventureMessage).getDriver2Card(),
                            SensorType.Driver2Card, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage511) inventureMessage).getDriver1TimeRelativeStates(),
                    SensorType.Driver1TimeRelativeStates, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage511) inventureMessage).getDriver2TimeRelativeStates(),
                    SensorType.Driver2TimeRelativeStates, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage511) inventureMessage).getTachgraphPerformance(),
                    SensorType.TachgraphPerformance, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage511) inventureMessage).getHandlingInformation(),
                    SensorType.HandlingInformation, time));
            sensorDatas
                    .add(new SensorData(((InventureMessage511) inventureMessage).getSystemEvent(),
                            SensorType.SystemEvent, time));
            sensorDatas.add(new SensorData(
                    ((InventureMessage511) inventureMessage).getTachogrVehicleSpeed(),
                    SensorType.TachogrVehicleSpeed, time));
        } else if (inventureMessage instanceof InventureMessage512) {
            sensorDatas.add(new SensorData(
                    ((InventureMessage512) inventureMessage).getEngineCoolantTemperature(),
                    SensorType.EngineCoolantTemperature, time));
        }
        return sensorDatas;
    }

}
